#!/bin/bash

timebeat="timebeat-2.1.4-amd64.deb"
server="timebeat_linux_server.yml"
client="timebeat_linux_client.yml"
settings=""


usage () {
    echo "Usage: install_timebeat.sh [-h] [-client] [-server]"
    exit 0
}

# if no arguments are passed, print usage
if [ $# -eq 0 ]; then
    usage
fi

# read the arguments from the command line
# we should get -client or -server or -h
for var in "$@"
do
    if [ "$var" = "-h" ]; then
        usage
    elif [ "$var" = "-client" ]; then
        echo "Installing timebeat client"
        settings=$client
    elif [ "$var" = "-server" ]; then
        echo "Installing timebeat server"
        settings=$server
    else
        echo "Unknown argument: $var"
        exit 1
    fi
done

# Check to see if timebeat install package already exists
if [ -f "$timebeat" ]; then
    echo "timebeat install package already exists... Skipping"
else
    echo "timebeat install package does not exist... Downloading"
fi

# Download timebeat .deb
echo "Downloading timebeat..."
# build the link
link="https://license.timebeat.app/releases/$timebeat"
# download the file
wget $link

# Check if the file was downloaded
if [ ! -f "$timebeat" ]; then
    echo "Error: timebeat was not downloaded"
    exit 1
fi

# Check if we are on a x64 system
if [ $(uname -m) != "x86_64" ]; then
    echo "Error: This script is only for x64 systems"
    exit 1
fi

# Check if the timebeat is already installed
result=$(dpkg -l | grep timebeat)

# Install timebeat then verify it
if [ -z "$result" ]; then
    echo "Installing timebeat..."
    sudo dpkg -i $timebeat
else
    echo "timebeat is already installed... Skipping"
fi

# Capture the output of the command in a variable
result=$(dpkg -l | grep timebeat)

# Check if the result is empty
if [ -z "$result" ]; then
    echo "timebeat is not installed or not found."
else
    echo "Found timebeat:"
    echo "$result"
fi

# download the license file from s3
echo "Downloading license file..."
aws s3 cp s3://greyparrot-all-data/camera-boxes/licenses/timebeat.lic .

# copy the license file to the timebeat folder
echo "Installing license file..."
sudo cp timebeat.lic /etc/timebeat/

# copy the settings file to the timebeat folder
echo "Installing settings file..."
sudo cp $settings /etc/timebeat/timebeat.yml

# start the service
echo "Starting timebeat service..."
sudo service timebeat start

# Enable the service to start on boot
echo "Enabling timebeat service to start on boot..."
sudo systemctl enable timebeat

echo "Disabling NTP service..."
sudo systemctl stop ntp
sudo systemctl disable ntp
